# linux 下 FTP 指令操作篇

## 安装 ftp

```sh
$ apt install ftp
```

## FTP 指令说明

-   远程连接

    | 指令描述     | 指令                           |
    | ------------ | ------------------------------ |
    | 主动模式连接 | ftp [-46pinegvd] [host [port]] |
    | 被动模式连接 | pftp [-46inegvd] [host [port]] |

    | 属性 | 属性描述                                                                                |
    | ---- | --------------------------------------------------------------------------------------- |
    | -4   | 仅使用 IPv4 与服务器连接、数据操作                                                      |
    | -6   | 仅使用 IPv6 与服务器连接、数据操作                                                      |
    | -p   | 使用被动模式进行数据传输；如以 pftp 调用，这是默认值                                    |
    | -i   | 在多个文件传输期间关闭交互式提示                                                        |
    | -n   | 如有必要，提示输入密码和登录帐户                                                        |
    | -e   | 如果命令已编译到 ftp 可执行文件中，则禁用命令编辑和历史记录支持。另一方面，什么也不做。 |
    | -g   | 禁用文件名全局绑定                                                                      |
    | -v   | 强制 ftp 显示来自远程服务器的所有响应，并报告数据传输统计信息。                         |
    | -d   | 启用调试                                                                                |

-   FTP 用户指令集合

    ```text
    - 客户端主机和 ftp 与之通信的可选端口号可以在命令行
    - 如果这样做，ftp 将立即尝试与 FTP 服务器建立连接在该主机上；否则，ftp 将进入它的命令解释器并等待用户的指令
    - 当 ftp 正在等待来自用户的命令时，会向用户提供提示 `ftp>`
    - 以下命令被 ftp 识别：
    ```

    | 指令描述                | 指令                                         |
    | ----------------------- | -------------------------------------------- |
    | 默认传输类型            | ascii                                        |
    | 二进制传输类型          | binary                                       |
    | 更改远程目录            | cd <remote-dir>                              |
    | 进入远程父级目录        | cdup                                         |
    | 更改远程文件权限        | chmod <mode> <file-name>                     |
    | 关闭当前 FTP 服务器连接 | close 或 disconnect                          |
    | 本地文件上传到远程      | put [<local-dir>/]local-file [remote-file]   |
    | 下载远程文件            | get <remote-file> [<local-dir>/<local-file>] |
    | 删除远程文件            | delete <remote-file>                         |
    | 创建远程目录            | mkdir <dir-name>                             |
    | 删除远程目录            | rmdir <remote-dir>                           |
    | 打开一个远程连接        | open <host> [<port>]                         |
    | 打印远程目录            | pwd                                          |
    | 重命名远程文件          | rename <old-name> <new-name>                 |
    | 清除队列                | reset                                        |
    | 查看 ftp 连接状态       | status                                       |
    | 查看远程文件大小        | size <file-name>                             |
    | 更改远程的默认 umask    | umask [newmask]                              |
    | 切换 ftp 登录用户       | user user-name [password] [account]          |
    | 中断传输                | 快捷键 `Ctrl-C`                              |
