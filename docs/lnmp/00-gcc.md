# 构建 GCC

使用自己构建的 gcc 版本，确保更好的兼容性

-   gcc 官网：https://gcc.gnu.org/
-   gcc 科大镜像：http://mirrors.ustc.edu.cn/gnu/gcc/
-   gcc 清华镜像：https://mirrors.tuna.tsinghua.edu.cn/gnu/gcc/
