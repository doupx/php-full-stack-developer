# 配置 Debian — 服务器版

测试服务器是腾讯云的，Debian 最高版为 10.2

```sh
# 查看版本
$ cat /etc/os-release
PRETTY_NAME="Debian GNU/Linux 10 (buster)"
NAME="Debian GNU/Linux"
VERSION_ID="10"
VERSION="10 (buster)"
VERSION_CODENAME=buster
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
```

## 升级 Debian

升级到 Debian 11.0.x 版本

### 修改源文件的内容：

```sh
$ cp /etc/apt/sources.list{,.bak}
$ vim /etc/apt/sources.list
```

> 将 sources.list 内容修改成如下：

```conf
deb http://mirrors.tencentyun.com/debian/ bullseye main contrib non-free
deb-src http://mirrors.tencentyun.com/debian/ bullseye main contrib non-free

deb http://mirrors.tencentyun.com/debian/ bullseye-updates main contrib non-free
deb-src http://mirrors.tencentyun.com/debian/ bullseye-updates main contrib non-free

deb http://mirrors.tencentyun.com/debian/ bullseye-backports main contrib non-free
deb-src http://mirrors.tencentyun.com/debian/ bullseye-backports main contrib non-free

deb http://mirrors.tencentyun.com/debian-security/ bullseye-security main contrib non-free
deb-src http://mirrors.tencentyun.com/debian-security/ bullseye-security main contrib non-free
```

> 可以进一步缩减

```conf
deb http://mirrors.tencentyun.com/debian/ bullseye main
deb-src http://mirrors.tencentyun.com/debian/ bullseye main

deb http://mirrors.tencentyun.com/debian/ bullseye-updates main
deb-src http://mirrors.tencentyun.com/debian/ bullseye-updates main

deb http://mirrors.tencentyun.com/debian-security/ bullseye-security main
deb-src http://mirrors.tencentyun.com/debian-security/ bullseye-security main
```

### 更新指令

服务器是空的，我们采用完全更新

```sh
$ apt autoclean
$ apt update
$ apt full-upgrade
```

安装过程中会多次出现类似如下提示：我们选择 `Y`

```text
Configuration file '/etc/debian_version'
 ==> Modified (by you or by a script) since installation.
 ==> Package distributor has shipped an updated version.
   What would you like to do about it ?  Your options are:
    Y or I  : install the package maintainer's version
    N or O  : keep your currently-installed version
      D     : show the differences between the versions
      Z     : start a shell to examine the situation
 The default action is to keep your current version.
*** debian_version (Y/I/N/O/D/Z) [default=N] ?
```

字符代号说明：

| 字母 | 说明                  |
| ---- | --------------------- |
| Y/I  | 安装包维护者的版本    |
| N/O  | 保留当前安装的版本    |
| D    | 显示版本之间的差异    |
| Z    | 启动 shell 以检查情况 |

其它选择界面说明：

1. Configuring libc6:amd64

    选择 `Yes`

2. 安装突然停止

    一般是系统缺少扩展，系统会给我们作出提示，我这里提示的是通过如下指令继续安装

    ```sh
    $ apt --fix-broken install
    ```

3. apt --fix-broken install

    选择第一个 `install the package maintainer's version`

4. 安装突然停止

    使用 `apt upgrade` 继续更新

5. Configuring console-setup

    选择 `utf-8`

6. Modified configuration file

    选择第一个 `install the package maintainer's version`

7. 类似第一种界面提示，我们选择 `Y`

8. 安装突然停止

    使用 `apt upgrade` 继续更新

9. 类似第一种界面提示，我们选择 `Y`

10. 类似第一种界面提示，我们选择 `Y`

11. Configuring kdump-tools

    选择第一个 `install the package maintainer's version`

12. 清空不必要和过时的包

    使用 `apt autoremove`

### 未升级包处理

使用 `apt upgrade` 发现还有未升级软件包

```sh
$ apt upgrade
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Calculating upgrade... Done
The following packages have been kept back:
  cpp gcc libpython2-stdlib libpython2.7-minimal libpython2.7-stdlib libpython3-stdlib python-six python2
  python2-minimal python2.7 python2.7-minimal python3 python3-apt python3-chardet python3-distutils
  python3-lib2to3 python3-markupsafe python3-minimal python3-pycurl python3-six python3-yaml
0 upgraded, 0 newly installed, 0 to remove and 21 not upgraded.
```

解决方式，直接安装它们：

```sh
$ apt install cpp gcc libpython2-stdlib libpython2.7-minimal libpython2.7-stdlib libpython3-stdlib python-six python2
  python2-minimal python2.7 python2.7-minimal python3 python3-apt python3-chardet python3-distutils
  python3-lib2to3 python3-markupsafe python3-minimal python3-pycurl python3-six python3-yaml
```

### 大版本升级成功的标志

使用 `apt update` 出现如下提示，没有任何其它提示时，就代表完全更新完成

```sh
$ apt update
Hit:1 http://mirrors.tencentyun.com/debian bullseye InRelease
Hit:2 http://mirrors.tencentyun.com/debian bullseye-updates InRelease
Hit:3 http://mirrors.tencentyun.com/debian bullseye-backports InRelease
Hit:4 http://mirrors.tencentyun.com/debian-security bullseye-security InRelease
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
All packages are up to date.
```

### 重启

更新结束后，我们需要重启服务器

```sh
$ sync;sync;sync;reboot
```

## 关于软件源

如果使用外部软件源，安全组就需要开启相应权限，一般可以先直接开启全部的权限，等升级完成再开启安全组。

因此，云服务器的内网如果有软件源，一定是我们首选软件源！

> 到此，配置 debian——服务器版基本结束，如果不需要升级，基本上服务器版本是不需要过多处理的
