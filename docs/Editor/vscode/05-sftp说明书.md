# SFTP 说明书

-   name

    ```sh
    string -  用于标识您的配置的字符串。
    ```

-   context

    ```sh
    string -  相对于工作区根文件夹的路径。当您想将子文件夹映射到 remotePath 时使用它。
    默认值： /
    ```

-   protocol

    ```sh
    string -  sftp 或 ftp。
    默认值：sftp
    ```

-   host

    ```sh
    string -  服务器的主机名或 IP 地址。
    ```

-   port

    ```sh
    整数 - 服务器的端口号。
    默认值：22
    ```

-   username

    ```sh
    string -  用于身份验证的用户名。
    ```

-   password

    ```sh
    string -  基于密码的用户身份验证的密码。
    ```

-   remotePath

    ```sh
    string -  远程的绝对路径
    默认值：/
    ```

-   uploadOnSave

    ```sh
    boolean - 每次保存 VS 代码时上传
    默认值：false
    ```

-   downloadOnOpen

    ```sh
    boolean - 每次打开时从远程服务器下载文件
    默认值：false
    ```

-   syncOption

    ```sh
    object - 配置 Sync 命令的行为。
    默认值：{}
    ```

-   syncOption.delete

    ```sh
    boolean - 从目标目录中删除无关文件。
    ```

-   syncOption.skipCreate

    ```sh
    boolean - 跳过在 dest 上创建新文件。
    ```

-   syncOption.ignoreExisting

    ```sh
    boolean - 跳过更新 dest 上存在的文件。
    ```

-   syncOption.update

    ```sh
    boolean - 仅当 src 文件系统上有较新版本时才更新 dest。
    ```

-   ignore

    ```sh
    string [] - 与 gitignore 相同的行为，所有路径都相对于当前配置的上下文
    默认值：[]
    ```

-   ignoreFile

    ```sh
    string -  忽略文件的绝对路径或相对于工作区根文件夹的相对路径。
    ```

-   watcher

    ```sh
    文件忽略
    ```

-   watcher.files

    ```sh
    string - 在 VS cdoe 编辑器之外被观察和编辑的 glob 模式被处理。uploadOnSave 当您观看所有内容时设置为 false。
    ```

-   watcher.autoUpload

    ```sh
    boolean - 文件更改时上传
    ```

-   watcher.autoDelete

    ```sh
    boolean - 删除文件时删除
    ```

-   remoteTimeOffsetInHours

    ```sh
    number - 本地机器和远程/服务器之间的小时数差异。（远程减去本地）
    默认值：0
    ```

-   remoteExplorer

    ```sh
    远程目录起点
    ```

-   remoteExplorer.filesExclude

    ```sh
    string/[] - 配置排除文件和文件夹的模式。远程资源管理器根据此设置决定显示或隐藏哪些文件和文件夹。
    ```

-   concurrency

    ```sh
    数字 - 较低的并发性可以获得更高的稳定性，因为某些客户端/服务器具有某种配置/硬编码限制。
    默认值：4
    ```

-   connectTimeout

    ```sh
    number - 最大连接时间
    默认值：10000
    ```

-   limitOpenFilesOnRemote

    ```sh
    混合 - 将打开的文件描述符限制为远程服务器中的特定数量。设置为 true 以使用默认限制（222）。除非必须，否则不要设置此项。
    默认值：false
    ```
