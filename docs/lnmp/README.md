# LNMP 部署环境搭建

PHP 首选开发环境和部署环境都是 LNMP，下面我们从头开始学习 LNMP

## LNMP 是什么？

LNMP 是指一组用来运行动态网站或者服务器的自由软件名称首字母缩写，对于 PHP 开发者而言具体如下：

| 字母 | 描述          |
| ---- | ------------- |
| L    | Linux         |
| N    | Nginx         |
| M    | MySQL/MariaDB |
| P    | PHP           |

## LNMP 涉及哪些软件包

在我们本次的环境搭建中，LNMP 具体会包含以下内容：

-   在 Debian 11.x 下搭建 PHP 开发环境、部署环境
-   使用 php-fpm 和 Nginx 结合的 web 环境
-   MySQL/MariaDB 关联 PHP 的数据操作
-   adminer 网页版多种数据库管理工具的安装
-   phpMyAdmin 网页版 mysql 数据库管理工具的安装
-   phpRedisAdmin 网页版 redis 数据库管理工具安装

| 服务部包                                          | 其它包                                                           |
| ------------------------------------------------- | ---------------------------------------------------------------- |
| [nginx](http://nginx.org/)                        | [debian](https://www.debian.org/)                                |
| [php](https://www.php.net/downloads)              | [phpMyAdmin](https://www.phpmyadmin.net/)                        |
| [MariaDB](https://downloads.mariadb.org/mariadb/) | [adminer](https://www.adminer.org/)                              |
| [MySQL](https://dev.mysql.com/downloads/)         | [phpRedisAdmin](https://github.com/ErikDubbelboer/phpRedisAdmin) |

## 编译器

搭建 lnmp 环境我们使用的编译器通常都是 gcc，在选择 gcc 版本上主要有如下 2 种方式：

### 自己构建 gcc

自己构建 gcc 更加灵活和稳定，但是弊端也会很多

> 优势：

1. 自己选择 gcc 版本；
2. 自己定义目录，不会跟系统产生关联；
3. 系统更新对 LNMP 环境不会有任何影响。

> 劣势：

1.  构建 lnmp 需要的依赖库，都应该由新的 gcc 重新构建；
2.  这就需要我们对 nginx、php、MySQL/MariaDB 包非常熟悉；
3.  合理构建 gcc 也会是一个很大的难点。

### 使用发行版自带 gcc

使用发行版自带 gcc 搭建环境会比较简单，也是大多数开发者的选择

> 3 种选择：

1. 使用发行版自带的包，**稳定性最佳**；
2. 使用可信赖的机构，根据该发行版来构建的源；
3. 自己构建需要的包。

> 优势：

1. 省去对依赖库的构建和配置；
2. 对专业知识要求降低；

> 劣势：

1. 依赖库、gcc 甚至包版本都会随系统更新而更新，不受自己控制
2. 性能无法最大化，通常 gcc、依赖库、包都是最新版的性能是最佳的

## lnmp 包及其依赖列表

这里尽可能的列出各个软件包需要的依赖项目
