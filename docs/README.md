# 《PHP 全栈开发一本通》

本手册内容涵盖了前端和 PHP 所必备的基础知识点，前端部分主要依赖阮一峰写的文档

## 文档构建方式

-   文档格式以 markdown 为主
-   通过 loopo 的修改版本来构建静态文件
-   后续如果有更加适合的，将会替换构建方式

> 备注：loopo 系列由阮一峰编写，项目静态网站由 emad-loopo 系列构建

## 文档模块导航

这里列出的文档，将有作者自己提供和维护

| 模块导航                                   |
| ------------------------------------------ |
| [完全掌握 vscode](./vscode/README.md)      |
| [完全掌握 markdown](./markdown/README.md)  |
| [debian-服务器篇](./debian/README.md)      |
| [部署环境-lnmp 篇](./lnmp/README.md)       |
| [部署环境-docker 篇](./docker/README.md)   |
| [web 服务器-Nginx 篇](./nginx/README.md)   |
| [关系型数据库-mysql 篇](./mysql/README.md) |
| [php 包管理工具](./composer/README.md)     |
| [js 包管理工具](./npm/README.md)           |

## 学习大纲简述

该文档需结合阮一峰的网道课程来学习，全部内容以及学习顺序建议如下：

| 模块                             | 作者     |
| -------------------------------- | -------- |
| [VSCode](./vscode/README.md)     | 地上马   |
| [MarkDown](./markdown/README.md) | 地上马   |
| HTML                             | 阮一峰   |
| CSS                              | 阮一峰   |
| JavaScript                       | 阮一峰   |
| PHP                              | 官方文档 |
| [Debian](./debian/README.md)     | 地上马   |
| [LNMP](./lnmp/README.md)         | 地上马   |
| [Docker](./docker/README.md)     | 地上马   |
| ES6                              | 阮一峰   |
| vue3                             | 官方文档 |
| [Nginx](./nginx/README.md)       | 地上马   |
| [MySQL](./mysql/README.md)       | 地上马   |
| [Composer](./composer/README.md) | 地上马   |
| [NPM](./npm/README.md)           | 地上马   |
| ~~Node.js~~                      | 阮一峰   |
| ~~Git~~                          | 阮一峰   |
| ~~Bash~~                         | 阮一峰   |
| ~~SSH~~                          | 阮一峰   |

| 官方文档地址                                  |
| --------------------------------------------- |
| [PHP](https://www.php.net/manual/zh/)         |
| [vue3](https://v3.cn.vuejs.org/)              |
| [vite](https://cn.vitejs.dev/)                |
| [Git](https://git-scm.com/book/zh/v2)         |
| [VuePress](https://v2.vuepress.vuejs.org/zh/) |
