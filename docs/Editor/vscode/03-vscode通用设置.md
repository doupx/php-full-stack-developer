# vscode 通用设置

1. 编辑器自带的功能

    ```json
    {
        "workbench.startupEditor": "none",
        "editor.fontFamily": "'Fira Code', '思源黑体 CN'",
        "editor.fontLigatures": true,
        "editor.inlineHints.fontFamily": "'Fira Code', '思源黑体 CN'",
        "editor.lineNumbers": "relative",
        "editor.linkedEditing": true,
        "editor.tabSize": 4,
        "editor.wordWrap": "on",
        "files.autoGuessEncoding": true,
        "files.autoSave": "onFocusChange",
        "files.eol": "\n",
        "files.exclude": {
            "**/.git": true,
            "**/.svn": true,
            "**/.hg": true,
            "**/CVS": true,
            "**/.DS_Store": true
        },
        "files.trimFinalNewlines": true,
        "files.insertFinalNewline": true,
        "files.trimTrailingWhitespace": true,
        "files.watcherExclude": {
            "**/.git/objects/**": true,
            "**/.git/subtree-cache/**": true,
            "**/node_modules/*/**": true,
            "**/.hg/store/**": true
        },
        "search.exclude": {
            "**/node_modules": true,
            "**/bower_components": true,
            "**/*.code-search": true
        },
        "update.mode": "manual",
        "html.format.enable": false,
        "json.format.enable": false,
        "javascript.format.enable": false,
        "terminal.integrated.defaultProfile.windows": "Git Bash", // 选择「Git Bash」为默认终端
        "terminal.integrated.automationShell.windows": "C:\\Program Files\\Git\\bin\\bash.exe", // 自定义终端路径
        "workbench.iconTheme": "vscode-icons",
        "workbench.colorTheme": "One Dark Pro",
        "prettier.tabWidth": 4,
        "sync.gist": "gistid"
    }
    ```
