# Debian 常用指令

## find 指令

1. /server/www 目录下的所有子目录都设置成 750 权限

    ```sh
    $ find /server/www/* -type d -exec chmod 750 {} \;
    ```

2. /server/www 目录下的所有文件都设置成 640 权限

    ```sh
    $ find /server/www/* -type f -exec chmod 640 {} \;
    ```

## find 与 tar 组合

概要：搜索 `cnrhyy` 目录下的所有 `zt` 目录，并将其打包到 cnrhyy_zt.tar.gz 压缩文件下

1. find 自带的 `-exec` 指令

    `-exec` 会多次执行 `tar` 指令，应该采用增量打包（压缩不支持增量操作）

    ```sh
    $ cd /alidata/www/jjz_aji/zj_wz/
    $ find cnrhyy -type d -name "zt" -exec tar -rvf cnrhyy_zt.tar {} \;
    $ tar -czvf cnrhyy_zt.tar.gz cnrhyy_zt.tar
    ```

2. 使用 `xargs` 指令

    使用 `xargs` 指令，`tar` 指令只会执行（`xargs` 会将打印出来的换行符等空白符转成空格）

    ```sh
    $ cd /alidata/www/jjz_aji/zj_wz/
    $ find cnrhyy -type d -name "zt" | xargs tar -czvf cnrhyy_zt.tar.gz
    ```

## du 指令说明

Linux du （英文全拼：disk usage）命令用于显示目录或文件的大小。
du 会显示指定的目录或文件所占用的磁盘空间。

```text
- 指令：du -h[a] --max-depth=<number> [<path>]
- 描述：指定层数显示，文件及目录大小
```

1. 显示当前目录以及 1 级子目录的大小

    ```sh
    $ du -h --max-depth=1
        1.7M	./mysql
        4.0M	./yhz3_baike_gyt02_cn
        2.0G	./wzyhz_zjyy888_com
        1.5M	./yhz3_blog_gyt01_cn
        1.8G	./yhz_other_zjyy118_com
        4.1G	.
    ```

2. 显示当前目录、当前目录下文件以及 1 级子目录的大小

    ```sh
    $ du -ha --max-depth=1
        1.7M	./mysql
        48M	./ib_logfile0
        4.0M	./yhz3_baike_gyt02_cn
        2.0G	./wzyhz_zjyy888_com
        48M	./ib_logfile1
        3.6M	./aria_log.00000001
        333M	./ibdata1
        4.0K	./aria_log_control
        1.5M	./yhz3_blog_gyt01_cn
        4.0K	./mysql_upgrade_info
        0	./multi-master.info
        0	./debian-10.0.flag
        1.8G	./yhz_other_zjyy118_com
        4.1G	.
    ```

3. 显示指定目录、指定目录下文件以及 1 级子目录的大小

    ```sh
    $ du -ha --max-depth=1 /alidata/mysql
        1.7M	/alidata/mysql/mysql
        48M	/alidata/mysql/ib_logfile0
        4.0M	/alidata/mysql/yhz3_baike_gyt02_cn
        2.0G	/alidata/mysql/wzyhz_zjyy888_com
        48M	/alidata/mysql/ib_logfile1
        3.6M	/alidata/mysql/aria_log.00000001
        333M	/alidata/mysql/ibdata1
        4.0K	/alidata/mysql/aria_log_control
        1.5M	/alidata/mysql/yhz3_blog_gyt01_cn
        4.0K	/alidata/mysql/mysql_upgrade_info
        0	/alidata/mysql/multi-master.info
        0	/alidata/mysql/debian-10.0.flag
        1.8G	/alidata/mysql/yhz_other_zjyy118_com
        4.1G	/alidata/mysql/
    ```
