# 编译安装 Nginx

Nginx 是现如今性能最强劲的 Web 服务器及反向代理服务器

## 涉及软件包

Nginx 编译安装，所涉及到的软件包具体如下：

| 源码及地址                                                                 |
| -------------------------------------------------------------------------- |
| [nginx-1.20.1.tar.gz](http://nginx.org/en/download.html)                   |
| [openssl-1.1.1l.tar.gz](https://www.openssl.org/source/)                   |
| [pcre-8.43.tar.gz](https://sourceforge.net/projects/pcre/files/pcre/)      |
| [perl-5.34.0.tar.gz](https://www.activestate.com/products/perl/downloads/) |
| [zlib-1.2.11.tar.gz](http://www.zlib.net/)                                 |

> 提示：Nginx 官方说明里，支持的版本是 `4.4 — 8.43`

## 构建前

-   创建 nginx 用户及用户组

    ```sh
    $ useradd -c 'This is the nginx service user' -u 2002 -s /usr/sbin/nologin -d /server/www -M -U nginx
    ```

-   查看当前版本全部构建参数

    ```sh
    $ cd /package/lnmp/nginx-1.20.1
    $ ./configure --help
    ```

### 模块开发环境依赖

-   构建 http_xslt_module 模块需要 `libxml2/libxslt` 库

    ```sh
    $ apt install libxml2-dev
    $ apt install libxslt1-dev
    ```

-   构建 http_image_filter_module 模块需要 `GD` 库

    ```sh
    $ apt install libgd-dev
    ```

## 构建指令

```sh
$ cd /package/lnmp/nginx-1.20.1
$ ./configure \
--prefix=/server/nginx \
--builddir=/bulid/nginx \
--user=nginx\
--group=nginx \
--error-log-path=/server/logs/nginx/error.log \
--http-log-path=/server/logs/nginx/access.log \
--pid-path=/server/run/nginx.pid \
# 核心功能模块
--with-threads \
--with-file-aio \
# 启用http功能模块
--with-http_ssl_module \
--with-http_v2_module \
--with-http_realip_module \
--with-http_addition_module \
--with-http_xslt_module \
--with-http_image_filter_module \
--with-http_geoip_module \
--with-http_sub_module \
--with-http_dav_module \
--with-http_flv_module \
--with-http_mp4_module \
--with-http_gunzip_module \
--with-http_gzip_static_module \
--with-http_auth_request_module \
--with-http_random_index_module \
--with-http_secure_link_module \
--with-http_degradation_module \
--with-http_slice_module \
--with-http_stub_status_module \
--with-http_perl_module \
# 禁用http功能模块
--without-http_proxy_module \
--without-http_fastcgi_module \
--without-http_uwsgi_module \
--without-http_scgi_module \
--without-http_grpc_module \
--without-http_memcached_module \
--without-http_upstream_hash_module \
--without-http_upstream_ip_hash_module \
--without-http_upstream_least_conn_module \
--without-http_upstream_random_module \
--without-http_upstream_keepalive_module \
--without-http_upstream_zone_module \
# 外库路径
--with-perl=/package/lnmp/perl-5.34.0 \
--with-perl_modules_path=/package/lnmp/perl-5.34.0 \
--with-pcre=/package/pcre-8.43 \
--with-pcre-jit \
--with-zlib=/package/lnmp/zlib-1.2.11 \
--with-openssl=/package/lnmp/openssl-1.1.1l \
# 开启调试
--with-debug
```
