# 从源代码构建 nginx

nginx 构建是使用 configure 命令配置的，它定义了系统的各个方面，包括允许 nginx 用于连接处理的方法最后它创建了一个 Makefile。

## ningx 模块概念

nginx 共有 5 大功能（服务），每个功能又分若干模块

1. 核心功能：`Core functionality`
2. HTTP 功能：`ngx_http_{name}_module`
3. 邮箱功能：`ngx_mail_{name}_module`
4. 负载均衡功能：`ngx_stream_{name}_module`
5. 反向代理 Google 功能：`ngx_google_perftools_module`

动态模块概念

-   动态模块也是需要编译的
-   动态模块的优点是，需要用到的站点，在 nginx 配置文件中启用，才会加载

该 configure 命令支持以下参数：

## configure 基础参数

--help

-   打印帮助信息

--prefix=path

-   指定 nginx 安装目录；
-   默认设置为 /usr/local/nginx
-   nginx.conf 配置文件中设置的所有相对路径，都以当前目录为根目录
-   使用 configure 构建 nginx 时，除了库源的路径外，如果参数使用相对路径，也都以当前目录为根目录

--sbin-path=path

-   设置 nginx 可执行文件的名称
-   此名称仅在安装期间使用
-   默认情况下，文件名为 prefix/sbin/nginx

--modules-path=path

-   指定 nginx 动态模块的安装目录
-   默认情况下使用 prefix/modules 目录

--conf-path=path

-   设置 nginx.conf 配置文件的名称
-   默认情况下，文件名为 prefix/conf/nginx.conf
-   如果需要，nginx 始终可以使用不同的配置文件启动，方法是在命令行参数中指定它

    ```sh
    # 命令行：
    $ prefix/sbin/nginx -c filepath/file
    ```

--error-log-path=path

-   设置主要错误、警告和诊断文件的名称
-   默认情况下，文件名为 prefix/logs/error.log
-   安装后，始终可以 nginx.conf 使用 error_log 指令在配置文件中更改文件名

--pid-path=path

-   设置 nginx.pid 将存储主进程的进程 ID 的文件的名称
-   默认情况下，文件名为 prefix/logs/nginx.pid
-   安装后，始终可以 nginx.conf 使用 pid 指令在配置文件中更改文件名

--lock-path=path

-   为锁定文件的名称设置前缀
-   默认情况下，该值为 prefix/logs/nginx.lock
-   安装后，始终可以 nginx.conf 使用 lock_file 指令在配置文件中更改该值

--user=name

-   设置非特权用户的名称，其凭据将由工作进程使用
-   默认用户名是 nobody
-   安装后，始终可以 nginx.conf 使用 user 指令在配置文件中更改名称
-   不能指定为特权用户（通常是 root）

--group=name

-   设置工作进程将使用其凭据的组的名称
-   默认情况下，组名跟用户名称一致
-   安装后，始终可以 nginx.conf 使用 user 指令在配置文件中更改名称
-   不能指定为特权用户组（通常是 root）

--build=name

-   设置一个可选的 nginx 构建名称
-   字面理解：就是对 nginx 可执行文件进行重命名

--builddir=path

-   设置构建目录
-   默认构建在 configure 同一目录下
-   建议在源码根目录下新建 1 个子目录作为构建目录

    ```sh
    # 如：
    $ --builddir=/package/nginx-1.20.1/bulid \
    ```

## configure 模块参数

nginx 共有 5 大功能，每个功能又分若干模块

1. 核心功能：`Core functionality`
2. HTTP 服务功能：`ngx_http_{name}_module`
3. 邮箱服务功能：`ngx_mail_{name}_module`
4. 负载均衡功能：`ngx_stream_{name}_module`
5. 反向代理 Google 功能：`ngx_google_perftools_module`

模块启用、禁用参数说明

| 参数             | 描述       |
| ---------------- | ---------- |
| --with-模块名    | 启用该模块 |
| --without-模块名 | 禁用该模块 |

> 有些模块只有启用参数或禁用参数中的 1 种

### 核心功能可选参数

select_module 模块

-   启用或禁用构建允许服务器使用该 select() 方法的模块
-   如果平台不支持更合适的方法（例如 kqueue、epoll 或 /dev/poll），则会自动构建此模块

    | 可选参数                | 描述 |
    | ----------------------- | ---- |
    | --with-select_module    | 启用 |
    | --without-select_module | 禁用 |

poll_module 模块

-   启用或禁用构建允许服务器使用该 poll() 方法的模块
-   如果平台不支持更合适的方法（例如 kqueue、epoll 或 /dev/poll），则会自动构建此模块

    | 可选参数              | 描述     |
    | --------------------- | -------- |
    | --with-poll_module    | 启用构建 |
    | --without-poll_module | 禁用构建 |

thread_pool 模块

-   允许使用线程池

    | 可选参数       | 描述 |
    | -------------- | ---- |
    | --with-threads | 启用 |

file-aio 模块

-   允许在 Linux 上对文件异步输入输出(I/O)，简称：AIO

    | 可选参数        | 描述 |
    | --------------- | ---- |
    | --with-file-aio | 启用 |

### HTTP 功能可选参数

http_ssl_module 模块

-   此模块将 HTTPS 协议支持添加到 ngixn 的 HTTP 服务器
-   默认情况下不构建此模块
-   构建和运行此模块需要 OpenSSL 外库支持

    | 可选参数               | 描述 |
    | ---------------------- | ---- |
    | --with-http_ssl_module | 启用 |

http_v2_module 模块

-   允许构建一个支持 HTTP/2 的模块
-   默认情况下不构建此模块

    | 可选参数              | 描述 |
    | --------------------- | ---- |
    | --with-http_v2_module | 启用 |

http_realip_module 模块

-   该模块将客户端地址更改为在指定标头字段中发送的地址
-   简单说：就是获取真实 IP 地址
-   默认情况下不构建此模块

    | 可选参数                  | 描述 |
    | ------------------------- | ---- |
    | --with-http_realip_module | 启用 |

http_addition_module 模块

-   在响应前后添加文本
-   默认情况下不构建此模块

    | 可选参数                    | 描述 |
    | --------------------------- | ---- |
    | --with-http_addition_module | 启用 |

http_xslt_module 模块

-   该模块使用一个或多个 XSLT 样式表转换 XML 响应
-   默认情况下不构建此模块
-   构建和运行此模块需要 libxml2 和 libxslt 库

    | 可选参数                        | 描述 |
    | ------------------------------- | ---- |
    | --with-http_xslt_module         | 启用 |
    | --with-http_xslt_module=dynamic | 启用 |

http_image_filter_module 模块

-   用于转换 JPEG、GIF、PNG 和 WebP 格式的图像
-   默认情况下不构建此模块

    | 可选参数                                | 描述 |
    | --------------------------------------- | ---- |
    | --with-http_image_filter_module         | 启用 |
    | --with-http_image_filter_module=dynamic | 启用 |

http_geoip_module 模块

-   该模块基于 IP 地址匹配 MaxMind GeoIP 二进制文件，读取 IP 所在地域信息
-   默认情况下不构建此模块
-   应用 1：区别国内外作为 HTTP 访问规则
-   应用 2：区别国内城市地域作 HTTP 访问规则
-   简单说：主要就是用来限制区域访问

    | 可选参数                         | 描述 |
    | -------------------------------- | ---- |
    | --with-http_geoip_module         | 启用 |
    | --with-http_geoip_module=dynamic | 启用 |

http_sub_module 模块

-   该模块通过将一个指定的字符串替换为另一个来修改响应
-   默认情况下不构建此模块
-   举例说：本来原本是 abc，但是想让客户端看到的是 ABC

    | 可选参数               | 描述 |
    | ---------------------- | ---- |
    | --with-http_sub_module | 启用 |

http_dav_module 模块

-   该模块通过 WebDAV 协议，提供文件管理自动化
-   该模块处理 HTTP 和 WebDAV 的 PUT、DELETE、MKCOL、COPY 和 MOVE 方法
-   默认情况下不构建此模块

    | 可选参数               | 描述 |
    | ---------------------- | ---- |
    | --with-http_dav_module | 启用 |

http_flv_module 模块

-   该模块为 Flash 视频 (FLV) 文件提供伪流服务器端支持
-   默认情况下不构建此模块
-   简单说：是指允许客户端分段获取 FLV 文件数据，而不是加载整个文件

    | 可选参数               | 描述 |
    | ---------------------- | ---- |
    | --with-http_flv_module | 启用 |

http_mp4_module 模块

-   该模块为 MP4 文件提供伪流服务器端支持
-   默认情况下不构建此模块
-   简单说：是指允许客户端分段获取 MP4 文件数据，而不是加载整个文件

    | 可选参数               | 描述 |
    | ---------------------- | ---- |
    | --with-http_mp4_module | 启用 |

http_gzip_module 模块

-   该模块允许 nginx 在数据发送给客户端（如：浏览器）前，先将文件压缩成 gzip 格式
-   默认构建此模块
-   以 gzip 格式传输数据，可以提升用户体验，但是对服务器 cpu 消耗提高

    | 可选参数                   | 描述 |
    | -------------------------- | ---- |
    | --without-http_gzip_module | 禁用 |

http_gunzip_module 模块

-   该模块是对 http_gzip_module 模块的补充
-   该模块是针对 nginx 已经开启 gzip 压缩，而某些浏览器不支持 gzip 解压能力的情况
-   该模块允许服务器向客户端发送数据之前现将其解压，这些压缩数据可能来自于后端服务器压缩产生或者 nginx 服务器预压缩产生
-   默认情况下未构建此模块

    | 可选参数                  | 描述 |
    | ------------------------- | ---- |
    | --with-http_gunzip_module | 启用 |

http_gzip_static_module 模块

-   该模块是对 http_gzip_module 模块的补充
-   http_gzip_module 模块每次发送数据都要进行压缩，非常消耗 cpu
-   该模块允许服务器先发送 `a.html.gzip` 文件，而不是 `a.html` 文件
-   默认情况下未构建此模块

    | 可选参数                       | 描述 |
    | ------------------------------ | ---- |
    | --with-http_gzip_static_module | 启用 |

http_auth_request_module 模块

-   统一用户验证权限验证，应用于 `第三方认证` 和 `多个 nginx 服务器`
-   原理：收到请求后，先将该请求停住，生成子请求，子请求和该请求内容是相同的；通过反向代理技术把子请求传递给上游服务，根据上游服务的响应再来决定是否处理当前的请求
-   功能：向上游的服务器转发请求，若上游服务器返回的响应码是 2XX，则继续执行；若上游服务器返回的是 401 或者 403，则将响应返回给客户端
-   默认情况下未构建此模块

    | 可选参数                        | 描述 |
    | ------------------------------- | ---- |
    | --with-http_auth_request_module | 启用 |

http_random_index_module 模块

-   该模块处理以斜杠字符 `/` 结尾的请求，并在目录中选择一个随机文件作为索引文件
-   默认情况下不构建此模块
-   实际网站中，这个模块并不常用

    | 可选参数                        | 描述 |
    | ------------------------------- | ---- |
    | --with-http_random_index_module | 启用 |

http_secure_link_module 模块

-   该模块为 nginx 提供防盗链
-   该模块指定并允许检查请求的链接的真实性以及保护资源免遭未经授权的访问
-   该模块限制链接生效周期
-   默认情况下不构建此模块

    | 可选参数                       | 描述 |
    | ------------------------------ | ---- |
    | --with-http_secure_link_module | 启用 |

http_degradation_module 模块

-   内存不足时，nginx 将返回 204 或 444 给客户端
-   默认情况下不构建此模块

    | 可选参数                       | 描述 |
    | ------------------------------ | ---- |
    | --with-http_degradation_module | 启用 |

http_slice_module 模块

-   该模块将请求拆分为子请求，每个子请求都返回一定范围的响应
-   该模块提供更有效的大响应缓存
-   该模块应用于 CDN
-   默认情况下不构建此模块

    | 可选参数                 | 描述 |
    | ------------------------ | ---- |
    | --with-http_slice_module | 启用 |

http_stub_status_module 模块

-   该模块提供对基本状态信息的访问
-   默认情况下不构建此模块

    | 可选参数                       | 描述 |
    | ------------------------------ | ---- |
    | --with-http_stub_status_module | 启用 |

http_charset_module 模块

-   该模块将指定的字符集添加到 `Content-Type` 响应头字段，
-   并且可以另外将数据从一种字符集转换为另一种字符集
-   默认构建此模块

    | 可选参数                      | 描述 |
    | ----------------------------- | ---- |
    | --without-http_charset_module | 禁用 |

http_ssi_module 模块

-   该模块在通过它的响应中处理 SSI（服务器端包含）命令
-   默认构建此模块

    | 可选参数                  | 描述 |
    | ------------------------- | ---- |
    | --without-http_ssi_module | 禁用 |

http_userid_module 模块

-   该模块来设置适合客户端识别的 cookie
-   默认构建此模块

    | 可选参数                     | 描述 |
    | ---------------------------- | ---- |
    | --without-http_userid_module | 禁用 |

http_access_module 模块

-   该模块允许限制对某些客户端地址的访问
-   默认构建此模块

    | 可选参数                     | 描述 |
    | ---------------------------- | ---- |
    | --without-http_access_module | 禁用 |

http_auth_basic_module 模块

-   该模块允许通过使用 `HTTP 基本身份验证` 协议验证用户名和密码来限制对资源的访问
-   默认构建此模块

    | 可选参数                         | 描述 |
    | -------------------------------- | ---- |
    | --without-http_auth_basic_module | 禁用 |

http_mirror_module 模块

-   该模块通过创建后台镜像子请求来实现原始请求的镜像
-   默认构建此模块

    | 可选参数                     | 描述 |
    | ---------------------------- | ---- |
    | --without-http_mirror_module | 禁用 |

http_autoindex_module 模块

-   该模块处理以斜杠字符('/')结尾的请求，并在 `ngx_http_index_module` 模块找不到索引文件的情况下生成目录列表
-   默认构建此模块

    | 可选参数                        | 描述 |
    | ------------------------------- | ---- |
    | --without-http_autoindex_module | 禁用 |

http_geo_module 模块

-   该模块根据客户端 IP 地址创建具有值的变量
-   默认构建此模块

    | 可选参数                  | 描述 |
    | ------------------------- | ---- |
    | --without-http_geo_module | 禁用 |

http_map_module 模块

-   该模块根据其他变量的值创建具有值的变量
-   默认构建此模块

    | 可选参数                  | 描述 |
    | ------------------------- | ---- |
    | --without-http_map_module | 禁用 |

http_split_clients_module 模块

-   该模块创建适合 A/B 测试（也称为拆分测试）的变量
-   默认构建此模块

    | 可选参数                            | 描述 |
    | ----------------------------------- | ---- |
    | --without-http_split_clients_module | 禁用 |

http_referer_module 模块

-   该模块可以阻止对 `Referer` 标头字段中具有无效值的请求的站点访问
-   nginx 防盗链应用之一
-   默认构建此模块

    | 可选参数                      | 描述 |
    | ----------------------------- | ---- |
    | --without-http_referer_module | 禁用 |

http_rewrite_module 模块

-   允许 HTTP 服务器重定向请求和更改请求 URI 的模块
-   需要 PCRE 库来构建和运行此模块
-   默认构建此模块

    | 可选参数                      | 描述 |
    | ----------------------------- | ---- |
    | --without-http_referer_module | 禁用 |

http_proxy_module 模块

-   该模块允许将请求传递到另一台服务器
-   php 反向代理需要
-   默认构建此模块

    | 可选参数                    | 描述 |
    | --------------------------- | ---- |
    | --without-http_proxy_module | 禁用 |

http_fastcgi_module 模块

-   该模块允许将请求传递给 FastCGI 服务器
-   默认构建此模块

    | 可选参数                      | 描述 |
    | ----------------------------- | ---- |
    | --without-http_fastcgi_module | 禁用 |

http_uwsgi_module 模块

-   该模块允许将请求传递给 uwsgi 服务器
-   默认构建此模块

    | 可选参数                    | 描述 |
    | --------------------------- | ---- |
    | --without-http_uwsgi_module | 禁用 |

http_scgi_module 模块

-   该模块允许将请求传递给 SCGI 服务器
-   默认构建此模块

    | 可选参数                   | 描述 |
    | -------------------------- | ---- |
    | --without-http_scgi_module | 禁用 |

http_grpc_module 模块

-   该模块允许将请求传递给 gRPC 服务器
-   默认构建此模块

    | 可选参数                   | 描述 |
    | -------------------------- | ---- |
    | --without-http_grpc_module | 禁用 |

http_memcached_module 模块

-   该模块允许从 memcached 服务器获取响应
-   默认构建此模块

    | 可选参数                        | 描述 |
    | ------------------------------- | ---- |
    | --without-http_memcached_module | 禁用 |

http_limit_conn_module 模块

-   该模块限制每个键的连接数，例如，来自单个 IP 地址的连接数
-   默认构建此模块

    | 可选参数                         | 描述 |
    | -------------------------------- | ---- |
    | --without-http_limit_conn_module | 禁用 |

http_limit_req_module 模块

-   该模块限制每个键的请求处理率，例如，来自单个 IP 地址的请求的处理率
-   默认构建此模块

    | 可选参数                        | 描述 |
    | ------------------------------- | ---- |
    | --without-http_limit_req_module | 禁用 |

http_empty_gif_module 模块

-   该模块在内存中生成并发送单像素透明 GIF
-   默认构建此模块

    | 可选参数                        | 描述 |
    | ------------------------------- | ---- |
    | --without-http_empty_gif_module | 禁用 |

http_browser_module 模块

-   该模块创建的变量的值取决于 `"User-Agent"` 请求头字段的值
-   默认构建此模块

    | 可选参数                      | 描述 |
    | ----------------------------- | ---- |
    | --without-http_browser_module | 禁用 |

http_upstream_hash_module 模块

-   实现负载均衡 hash 方法的模块
-   默认构建此模块

    | 可选参数                            | 描述 |
    | ----------------------------------- | ---- |
    | --without-http_upstream_hash_module | 禁用 |

http_upstream_ip_hash_module 模块

-   实现负载均衡 ip_hash 方法的模块
-   默认构建此模块

    | 可选参数                               | 描述 |
    | -------------------------------------- | ---- |
    | --without-http_upstream_ip_hash_module | 禁用 |

http_upstream_least_conn_module 模块

-   实现负载均衡 least_conn 方法的模块
-   默认构建此模块

    | 可选参数                                  | 描述 |
    | ----------------------------------------- | ---- |
    | --without-http_upstream_least_conn_module | 禁用 |

http_upstream_random_module 模块

-   实现负载均衡 random 方法的模块
-   默认构建此模块

    | 可选参数                              | 描述 |
    | ------------------------------------- | ---- |
    | --without-http_upstream_random_module | 禁用 |

http_upstream_keepalive_module 模块

-   该模块提供与上游服务器的连接缓存
-   默认构建此模块

    | 可选参数                                 | 描述 |
    | ---------------------------------------- | ---- |
    | --without-http_upstream_keepalive_module | 禁用 |

http_upstream_zone_module 模块

-   该模块可以将上游组的运行时状态存储在共享内存区域中
-   默认构建此模块

    | 可选参数                            | 描述 |
    | ----------------------------------- | ---- |
    | --without-http_upstream_zone_module | 禁用 |

http_perl_module 模块

-   允许构建嵌入式 Perl 模块
-   默认情况下不构建此模块
-   据说会降低性能

    | 可选参数                        | 描述          |
    | ------------------------------- | ------------- |
    | --with-http_perl_module         | 启用          |
    | --with-http_perl_module=dynamic | 启用          |
    | --with-perl_modules_path=path   | Perl 模块目录 |
    | --with-perl=path                | Perl 源码路径 |

其它参数

```
--http-log-path=path
    设置 HTTP 服务器的主请求日志文件的名称
    安装后，始终可以 nginx.conf 使用 access_log 指令在配置文件中 更改文件名
    默认情况下，文件名为 prefix/logs/access.log

--http-client-body-temp-path=path
    定义一个目录，用于存储保存客户端请求正文的临时文件
    安装后，始终可以 nginx.conf 使用 client_body_temp_path 指令在配置文件中更改目录
    默认情况下，目录名为 prefix/client_body_temp

--http-proxy-temp-path=path
    定义一个目录，用于存储从代理服务器接收到的数据的临时文件
    安装后，始终可以 nginx.conf 使用 proxy_temp_path 指令在配置文件中更改目录
    默认情况下，目录名为 prefix/proxy_temp

--http-fastcgi-temp-path=path
    定义一个目录，用于存储从 FastCGI 服务器接收到的数据的临时文件
    安装后，始终可以 nginx.conf 使用 fastcgi_temp_path 指令在配置文件中 更改目录
    默认情况下，目录名为 prefix/fastcgi_temp

--http-uwsgi-temp-path=path
    定义一个目录，用于存储从 uwsgi 服务器接收到的数据的临时文件
    安装后，始终可以 nginx.conf 使用 uwsgi_temp_path 指令在配置文件中更改目录
    默认情况下，目录名为 prefix/uwsgi_temp

--http-scgi-temp-path=path
    定义一个目录，用于存储从 SCGI 服务器接收到的数据的临时文件
    安装后，始终可以 nginx.conf 使用 scgi_temp_path 指令在配置文件中更改目录
    默认情况下，目录名为 prefix/scgi_temp

--without-http
    禁用 HTTP 服务器功能

--without-http-cache
    禁用 HTTP 缓存
```

### 邮件功能可选参数

邮件功能默认不开启

```
--with-mail
--with-mail=dynamic
    启用 POP3/IMAP4/SMTP 邮件代理服务器

--with-mail_ssl_module
    允许构建一个模块，将 SSL/TLS 协议支持添加到邮件代理服务器
    默认情况下不构建此模块
    构建和运行此模块需要 OpenSSL 库

--without-mail_pop3_module
    禁用邮件代理服务器中的POP3协议

--without-mail_imap_module
    禁用邮件代理服务器中的IMAP协议

--without-mail_smtp_module
    禁用邮件代理服务器中的SMTP协议
```

## 负载均衡功能可选参数

负载均衡功能默认不开启

```
--with-stream
--with-stream=dynamic
    允许构建 用于通用 TCP/UDP 代理和负载平衡的流模块
    默认情况下不构建此模块

--with-stream_ssl_module
    允许构建一个模块，将 SSL/TLS 协议支持添加到流模块
    默认情况下不构建此模块
    构建和运行此模块需要 OpenSSL 库

--with-stream_realip_module
    启用构建ngx_stream_realip_module 模块
    该模块将客户端地址更改为在 PROXY 协议标头中发送的地址
    默认情况下不构建此模块

--with-stream_geoip_module
--with-stream_geoip_module=dynamic
    启用构建ngx_stream_geoip_module 模块
    该模块根据客户端 IP 地址和预编译的MaxMind数据库创建变量
    默认情况下不构建此模块

--with-stream_ssl_preread_module
    启用构建ngx_stream_ssl_preread_module 模块
    该模块允许在 不终止 SSL/TLS 的情况下从ClientHello消息中提取信息
    默认情况下不构建此模块

--without-stream_limit_conn_module
    禁用构建ngx_stream_limit_conn_module 模块
    该模块限制每个键的连接数
    例如，来自单个 IP 地址的连接数

--without-stream_access_module
    禁止构建ngx_stream_access_module 模块
    该模块允许限制对某些客户端地址的访问

--without-stream_geo_module
    禁用构建ngx_stream_geo_module 模块
    该模块根据客户端 IP 地址创建具有值的变量

--without-stream_map_module
    禁止构建ngx_stream_map_module 模块
    该模块根据其他变量的值创建具有值的变量

--without-stream_split_clients_module
    禁止构建 stream_split_clients_module 模块
    该模块为 A/B 测试创建变量

--without-stream_return_module
    该模块向客户端发送一些指定的值，然后关闭连接

--without-stream_set_module
    禁止构建 ngx_stream_set_module 模块
    该模块为变量设置值

--without-stream_upstream_hash_module
    禁用构建 stream_upstream_hash_module 模块
    该模块实现负载平衡的 hash 方法

--without-stream_upstream_least_conn_module
    禁用构建 stream_upstream_least_conn_module 模块
    该模块实现负载平衡的 least_conn 方法

--without-stream_upstream_random_module
    禁用构建 stream_upstream_random_module 模块
    该模块实现负载平衡的 random 方法

--without-stream_upstream_zone_module
    禁用构建 stream_upstream_zone_module 模块
    该模块可以将上游组的运行时状态存储在共享内存区域中
```

### 反向代理 Google 功能

默认不启用该功能

```
--with-google_perftools_module
    - 启用构建ngx_google_perftools_module 模块
    - 该模块启用使用Google Performance Tools对 nginx 工作进程进行分析
    - 该模块供 nginx 开发人员使用，默认情况下未构建
```

## 其它功能

```
--with-cpp_test_module
    允许构建 ngx_cpp_test_module模块

--add-module=path
    启用外部模块
    nginx自带模块： 使用 --with-module，不需要指定路径
    第三方模块：使用 --add-module=源码路径

--add-dynamic-module=path
    启用外部动态模块

--with-compat
    启用动态模块兼容性

--with-cc=path
    设置 C 编译器的名称

--with-cpp=path
    设置 C 预处理器的名称

--with-cc-opt=parameters
    设置将添加到 CFLAGS 变量的附加参数在 FreeBSD 下使用系统 PCRE 库时
    --with-cc-opt="-I /usr/local/include" 应指定如果select()需要增加支持的文件数量，也可以在此处指定
    例如： --with-cc-opt="-D FD_SETSIZE=2048"

--with-ld-opt=parameters
    设置将在链接期间使用的附加参数在 FreeBSD 下使用系统 PCRE 库时， --with-ld-opt="-L /usr/local/lib" 应指定

--with-cpu-opt=cpu
    启用每个指定 CPU 的构建： pentium, pentiumpro, pentium3, pentium4, athlon, opteron, sparc32, sparc64, ppc64

--without-pcre
    禁用 PCRE 库的使用

--with-pcre
    强制使用 PCRE 库

--with-pcre=path
    设置 PCRE 库源的路径需要从PCRE站点下载并提取库分发版（版本 4.4 — 8.43） 其余的由 nginx./configure和 make. location指令和 ngx_http_rewrite_module 模块中的正则表达式支持需要该库

--with-pcre-opt=parameters
    为 PCRE 设置额外的构建选项

--with-pcre-jit
    使用“即时编译”支持（1.1.12，pcre_jit指令）构建 PCRE 库

--with-zlib=path
    设置 zlib 库源的路径需要从zlib站点下载并提取库分发（版本 1.1.3 — 1.2.11） 其余的由 nginx./configure和 make. ngx_http_gzip_module模块需要该库

--with-zlib-opt=parameters
    为 zlib 设置额外的构建选项

--with-zlib-asm=cpu
    允许使用针对指定 CPU 之一优化的 zlib 汇编程序源： pentium, pentiumpro.

--with-libatomic
    强制使用 libatomic_ops 库

--with-libatomic=path
    设置 libatomic_ops 库源的路径

--with-openssl=path
    设置 OpenSSL 库源的路径

--with-openssl-opt=parameters
    为 OpenSSL 设置额外的构建选项

--with-debug
    启用调试日志
```
