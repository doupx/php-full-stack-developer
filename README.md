# markdown 转 html 说明

使用阮一峰的 loppo 工具，将 markdown 文档输出问 html 站点页面

-   本人使用的 npm 包是修改至阮一峰的 loppo 系列

> 阮一峰的原始包文件列表

| #   | 包名                          | 包说明         |
| --- | ----------------------------- | -------------- |
| 01  | loppo                         | 基础包         |
| 02  | loppo-theme-oceandeep         | 样式基础包     |
| 03  | loppo-theme-wangdoc           | 文档内容样式包 |
| 04  | loppo-theme-wangdoc-frontpage | 站点首页样式包 |

> 我修改后的包名

| #   | 包名                               | 包说明         |
| --- | ---------------------------------- | -------------- |
| 01  | emad-loppo                         | 基础包         |
| 02  | emad-loppo-theme-oceandeep         | 样式基础包     |
| 03  | emad-loppo-theme-wangdoc           | 文档内容样式包 |
| 04  | emad-loppo-theme-wangdoc-frontpage | 站点首页样式包 |

## 安装说明

1. 基础包是必装的
2. 样式基础包，是基础包的关联包，安装基础包时，自动安装
3. 文档内容样式包，用于生成文档
4. 站点首页样式包，用于生成首页样式，就 1 个首页，完全可以自己手工完成

> 文档包说明

| #   | 文档包名                 | 作者   |
| --- | ------------------------ | ------ |
| 01  | html-tutorial            | 阮一峰 |
| 02  | css-tutorial             | 阮一峰 |
| 03  | javascript-tutorial      | 阮一峰 |
| 04  | es6-tutorial             | 阮一峰 |
| 05  | webapi-tutorial          | 阮一峰 |
| 06  | ssh-tutorial             | 阮一峰 |
| 07  | bash-tutorial            | 阮一峰 |
| 08  | php-full-stack-developer | 地上马 |

> 首页包说明

| #   | 文档包名       | 作者                   |
| --- | -------------- | ---------------------- |
| 01  | frontpage      | 阮一峰                 |
| 02  | emad-frontpage | 修改阮一峰的网到首页包 |

## Git 初始化

新建的文档仓库，应该命名为`xxx-tutorial`。其中，`xxx`为该仓库的主题，比如`javascript-tutorial`。

将这个仓库进行 Git 初始化。

```sh
$ git init
```

然后，新建`.gitignore`，写入下面的内容。

```sh
.idea/
.vscode/
node_modules/
dist/
.DS_Store
package-lock.json
```

## npm 模块初始化

1. 第一步，新建`package.json`。

    ```sh
    $ npm init -y
    ```

2. 第二步，修改`package.json`。

    `package.json`的`license`字段可以改成创意共享许可证（署名-相同方式共享）。

    ```json
    "license": "CC-BY-SA-4.0",
    ```

    `scripts`字段下插入如下脚本。

    ```json
    "scripts": {
        "build": "emad-loppo --site \"[xxx] 教程\" --id [xxx] --theme wangdoc",
        "build-and-commit": "npm run build && npm run commit",
        "commit": "gh-pages --dist dist --dest dist/[xxx] --branch master --repo git@github.com:linjialiang/emad-website.git",
        "chapter": "emad-loppo chapter",
        "server": "emad-loppo server"
    },
    "husky": {
      "hooks": {
        "pre-push": "npm update"
      }
    },
    ```

    > 注意，要把脚本里面的`[xxx]`替换掉，共有三处。

3. 第三步，安装依赖。需要以下四个库

    - emad-loppo
    - emad-loppo-theme-wangdoc
    - gh-pages
    - husky

    ```sh
    $ npm install --save emad-loppo@latest emad-loppo-theme-wangdoc@latest gh-pages@latest husky@4.3.8
    ```

    注意，husky 固定为 4.3.8 版本，不再升级了。

    为了确保安装的是最新版本，可以打开`package.json`，将`loppo`和`loppo-theme-wangdoc`的版本改成`latest`，然后执行一下`npm update`。

    > 直接在 `package.json` 中加入这些数据就可以省略第三步的上面步骤

    ```json
    "dependencies": {
      "gh-pages": "latest",
      "husky": "^4.3.8",
      "emad-loppo": "latest",
      "emad-loppo-theme-wangdoc": "latest"
    }
    ```

    ```sh
    $ npm update
    ```

4. 第四步，运行`npm run chapter`，生成目录文件`chapters.yml`。

    ```sh
    $ npm run chapter
    ```

    编辑`chapters.yml`文件，使得目录编排正确。

5. 第五步，本地运行`npm run build && npm run server`，看看构建是否正确。

    ```sh
    npm run build
    npm run server
    ```

6. 第六步，写入代码仓库

    ```sh
    $ git add -A
    $ git commit -m "feat: first commit"
    ```

## GitHub 仓库（我没有处理）

在 GitHub 的 wangdoc 团队下新建仓库，然后推送本地仓库。

## GitHub Actions

第一步，新建子目录`.github/workflows`。

```sh
$ mkdir -p .github/workflows
```

第三步，检查仓库设置的`secrets`里面，有没有`WANGDOC_BOT_TOKEN`这一项。如果没有，需要为 wangdoc-bot 生成一个 TOKEN 并添加在`secrets`里面。

第三步，新建配置文件`wangdoc.yml`。

```sh
$ vi .github/workflows/wangdoc.yml
```

文件内容如下。

```yaml
name: [XXX] tutorial CI
on:
  push:
    branches:
      - main

jobs:
  page-generator:
    name: Generating pages
    runs-on: ubuntu-18.04
    steps:
      - name: Checkout
        uses: actions/checkout@v2
        with:
          persist-credentials: false
      - name: Setup Node.js
        uses: actions/setup-node@main
        with:
          node-version: '14'
      - name: Install dependencies
        run: npm install
      - name: Build pages
        run: npm run build
      - name: Deploy to website
        uses: JamesIves/github-pages-deploy-action@3.7.1
        with:
          GIT_CONFIG_NAME: wangdoc-bot
          GIT_CONFIG_EMAIL: yifeng.ruan@gmail.com
          REPOSITORY_NAME: wangdoc/website
          ACCESS_TOKEN: ${{ secrets.WANGDOC_BOT_TOKEN }}
          BASE_BRANCH: main
          BRANCH: master # The branch the action should deploy to.
          FOLDER: dist # The folder the action should deploy.
          TARGET_FOLDER: dist/[XXX]
          CLEAN: true # Automatically remove deleted files from the deploy branch
          COMMIT_MESSAGE: update
```

注意，将上面的`[XXX]`改成当前库。

## Travis-CI 构建

注意，如果使用 GitHub Actions，则不需要设置 Travis-CI。

第一步，到 [Travis CI 的官网](https://travis-ci.com/organizations/wangdoc/repositories)，关联该仓库，开启自动构建。

注意，要到设置里面，把 Pull Request 触发自动构建的选项关掉。

第二步，在项目根目录下，新建`.travis.yml`，写入以下内容。

```yml
language: node_js
node_js:
    - "node"

branches:
    only:
        - master

install:
    - npm ci
# keep the npm cache around to speed up installs
cache:
    directories:
        - "$HOME/.npm"

script: bash ./deploy.sh
env:
    global:
        - ENCRYPTION_LABEL: [xxxxxx]
        - COMMIT_AUTHOR_EMAIL: yifeng.ruan@gmail.com
```

注意，上面代码中的`[xxxxxx]`要等到第四步进行替换。

第三步，项目根目录下，新建`deploy.sh`，写入以下内容。

```sh
#!/bin/bash
set -e # Exit with nonzero exit code if anything fails

# Get the deploy key by using Travis's stored variables to decrypt deploy_key.enc
ENCRYPTED_KEY_VAR="encrypted_${ENCRYPTION_LABEL}_key"
ENCRYPTED_IV_VAR="encrypted_${ENCRYPTION_LABEL}_iv"
ENCRYPTED_KEY=${!ENCRYPTED_KEY_VAR}
ENCRYPTED_IV=${!ENCRYPTED_IV_VAR}
openssl aes-256-cbc -K $ENCRYPTED_KEY -iv $ENCRYPTED_IV -in wangdoc-deploy-rsa.enc -out wangdoc-deploy-rsa -d
chmod 600 wangdoc-deploy-rsa
eval `ssh-agent -s`
ssh-add wangdoc-deploy-rsa

# Now that we're all set up, we can push.
# git push $SSH_REPO $TARGET_BRANCH
npm run build-and-commit
```

第四步（待验证，可以跳过这步，直接执行下一个命令），在项目根目录下，添加私钥`wangdoc-deploy-rsa`。

```sh
$ cp ~/.ssh/wangdoc-deploy-rsa .
```

然后，使用`travis`命令加密`wangdoc-deploy-rsa`。如果没有安装 travis ci 的命令行客户端，可以参考官方的[安装文档](https://github.com/travis-ci/travis.rb#installation)`sudo gem install travis`。

```sh
$ travis encrypt-file ~/.ssh/wangdoc-deploy-rsa
```

屏幕上会显示加密编号，将这个编号写入`.travis.yml`。可以参考这个[范例文件](https://github.com/wangdoc/javascript-tutorial/blob/master/.travis.yml)。

第五步，重要！此时一定要删掉私钥`wangdoc-deploy-rsa`，只留下`wangdoc-deploy-rsa.enc`。

```sh
$ rm wangdoc-deploy-rsa
```

## Disqus 讨论区

第一步，到 Disqus 新建讨论区，讨论区的 slug 为`wangdoc-[id]`。

第二步，设好 Disqus 以后，在`loppo.yml`里面设定如下设置。

```yaml
hasComments: true|false
isTutorial: true|false
```
