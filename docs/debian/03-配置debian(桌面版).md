# 配置 Debian —— 桌面版

## 中文社区源安装

使用中科大的源速度比较快，具体操作如下：

```sh
# root 账户下处理更加方便
$ echo "deb http://mirrors.ustc.edu.cn/debiancn/ bullseye main" | tee /etc/apt/sources.list.d/debiancn.list
$ wget http://mirrors.ustc.edu.cn/debiancn/debiancn-keyring_0~20161212_all.deb -O /tmp/debiancn-keyring.deb
$ apt install /tmp/debiancn-keyring.deb
$ apt update
$ rm /tmp/debiancn-keyring.deb
```
