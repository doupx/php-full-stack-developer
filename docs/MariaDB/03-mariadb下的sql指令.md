# MariaDB 下的 sql 指令

## 备份数据库

| 说明           | 指令                                                                         |
| -------------- | ---------------------------------------------------------------------------- |
| 备份单个数据库 | $ mysqldump -uroot -p<密码> --databases <数据库名> > [<路径>]/<数据库名.sql> |
| 备份所有数据库 | $ mysqldump -uroot -p<密码> --all-databases > [<路径>]/<all_db.sql>          |
| 导回所有数据库 | $ mysql -uroot -p<密码> < [<路径>]/<all_db.sql>                              |
| 删除指定数据库 | $ MariaDB> drop database [<路径>]/<数据库名>;                                |
| 恢复单独数据库 | $ MariaDB> source <数据库名.sql>;                                            |
| 查看数据库列表 | $ MariaDB> show databases;                                                   |
