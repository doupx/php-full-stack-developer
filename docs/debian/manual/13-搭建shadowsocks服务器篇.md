# 搭建 shadowsocks 服务器篇

shadowsocks 是一个可穿透防火墙的快速代理

## 安装说明

-   Debian8.x:

    ```sh
    $ apt-get install shadowsocks
    ```

-   Debian10.x:

    ```sh
    $ apt install shadowsocks
    ```

## Debian8.x 配置文件

Debian8.x 的配置文件固定存放在于 /etc/shadowsocks/config.json

-   单用户配置文件：

    ```text
    {
        "server":"0.0.0.0",
        "server_port":8388,
        "local_address": "127.0.0.1",
        "local_port":1080,
        "password":"mypassword",
        "timeout":300,
        "method":"aes-256-cfb",
        "fast_open": false
    }
    ```

-   多用户配置文件：

    ```text
    {
        "server":"0.0.0.0",
        "port_password":{
            "8388": "mypassword1",
            "8389": "mypassword2"
        },
        "local_address": "127.0.0.1",
        "local_port":1080,
        "timeout":300,
        "method":"aes-256-cfb",
        "fast_open": false
    }
    ```

-   配置说明

    | 名称          | 解释                            |
    | ------------- | ------------------------------- |
    | server        | 您的服务器侦听的地址            |
    | server_port   | 服务器端口                      |
    | local_address | 您本地收听的地址                |
    | local_port    | 本地端口                        |
    | password      | 用于加密的密码                  |
    | timeout       | 最长响应时间                    |
    | method        | 默认值：“aes-256-cfb”，参见加密 |
    | fast_open     | 使用 TCP_FASTOPEN，true / false |
    | workers       | Unix/Linux 上可用的工作线程数   |

## 启动运行

使用 apt 安装后，默认支持开机启动

| 功能 | 指令                        |
| ---- | --------------------------- |
| 启动 | service shadowsocks start   |
| 停止 | service shadowsocks stop    |
| 重启 | service shadowsocks restart |

## 客户端

debian8.x 服务器推荐使用 Shadowsocks-3.x 的 windows 客户端版本；

Shadowsocks-4.x 亲测，无法不能正常使用
