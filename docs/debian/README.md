# Debian 简介

我们必须先了解 Debian 到底是什么？

| Debian 3 大概念             |
| --------------------------- |
| 1、什么是 Debian？          |
| 2、什么是 GNU/Linux？       |
| 3、什么是 Debian GNU/Linux? |

## 什么是 Debian？

Debian 是一个致力于自由软件开发并宣扬自由软件基金会之理念的自愿者组织。

-   简单说：Debian 是一个自由的组织
-   Debian 组织的工作内容：Web 和 FTP 站点管理、图形设计、软件许可协议的法律分析、编写文档，当然，还有维护软件包。

## 什么是 GNU/Linux？

GNU/Linux 是操作系统，包括内核(linux)和其它程序(GNU)两部分。

-   Linux 内核：本身也属于操作系统，但是只提供最底层的处理模块，是无法给普通用户正常使用的
-   GNU 程序：由 GNU 工程(自由软件基金会)编写或者符合 GNU 标准的一些了软件

人们通常所说的 Linux 指的就是 `GNU/Linux`

## 什么是 Debian GNU/Linux?

将 Debian 哲学与方法论，GNU 工具集、Linux 内核，以及其他重要的自由软件结合在一起所构成的独特的软件发行版称为 Debian GNU/Linux。

-   我们经常讲的 Debian 指的就是 `Debian GNU/Linux`

## Debian 中文文档

在当下 Debian 官方有提供了几个非常不错的 [中文文档](https://www.debian.org/doc/index.zh-cn.html) ，可以供大家深入学习

| 中文 PDF 文档                                  |
| ---------------------------------------------- |
| [Debian 参考卡片](./docs/refcard.pdf)          |
| [Debian 参考手册](./docs/debian-reference.pdf) |
| [Debian 维护者指南](./docs/debmake-doc.pdf)    |

-   [安装手册](https://www.debian.org/releases/stable/amd64/)

## 总结：

Debian 我们这里主要讲解一些配置问题，更多基础、核心的知识还是需要通过官方手册来指导学习；

大家如果想要学好 Linux ，学会阅读手册是必不可少的一环!
